<?php

/**
 * Add category
 */
function tsext_register_taxonomies() {
	register_taxonomy( 'video_category', 'video_post', [
		'labels'            => [
			'name'          => __( 'Video Categories', 'twentyseventeenext' ),
			'singular_name' => __( 'Video Category', 'twentyseventeenext' ),
		],
		'hierarchical'      => true,
		'query_var'         => true,
		'rewrite'           => true,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
	] );
}

add_action( 'init', 'tsext_register_taxonomies' );

/**
 * Add new post type
 */
function tsext_register_post_types() {
	register_post_type( 'video_post', [
		'labels'             => [
			'name'          => __( 'Video Records', 'twentyseventeenext' ),
			'singular_name' => __( 'Video Record', 'twentyseventeenext' ),
			'menu_name'     => __( 'Video', 'twentyseventeenext' ),
			'add_new'       => __( 'Add Video', 'twentyseventeenext' ),
			'add_new_item'  => __( 'Add Video', 'twentyseventeenext' ),
		],
		'description'        => __( 'Video Records', 'twentyseventeenext' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'capability_type'    => 'post',
		'map_meta_cap'       => true,
		'hierarchical'       => false,
		'taxonomies'         => [ 'video_category' ],
		'rewrite'            => true,
		'query_var'          => false,
		'delete_with_user'   => true,
		'supports'           => [
			'title',
			'thumbnail',
			'editor',
			'revisions',
			'excerpt',
		],
	] );
}

add_action( 'init', 'tsext_register_post_types' );