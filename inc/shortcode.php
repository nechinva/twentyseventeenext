<?php

/**
 * Render content with video list
 *
 * @param integer $category
 *
 * @return string
 */
function tsext_render_video_content( $category = 0 ) {
	$content = "";

	$params = [
		'numberposts' => - 1,
		'post_type'   => 'video_post',
	];

	if ( $category ) {
		$params['tax_query'] = [
			[
				'taxonomy' => 'video_category',
				'field'    => 'term_id',
				'terms'    => $category
			]
		];
	}

	$posts = get_posts( $params );

	if ( $posts ) {
		foreach ( $posts as $post ) {
			$content .= "<div>";
			$content .= "<strong><a href='" . get_permalink( $post ) . "'>" . get_the_title( $post ) . "</a></strong>";
			$content .= "</div>";
		}
	} else {
		$content .= "<div>";
		$content .= "<strong>" . __( 'No videos found', 'twentyseventeenext' ) . "</strong>";
		$content .= "</div>";
	}

	return $content;
}

/**
 * Render content for shortcode
 *
 * @return string
 */
function tsext_render_shortcode() {
	$content = "<div class='row'>";

	$categories = get_categories( [
		'type'     => 'video_post',
		'taxonomy' => 'video_category',
	] );

	if ( $categories ) {
		$content .= "<div>";
		$content .= "<label>" . __( 'Video Categories', 'twentyseventeenext' );
		$content .= "<select id='tsext-category'>";
		$content .= "<option value='0'>...</option>";
		foreach ( $categories as $cat ) {
			$content .= "<option value='{$cat->term_id}'>{$cat->name}</option>";
		}
		$content .= "</select>";
		$content .= "</label>";
		$content .= "</div>";
	}

	$content .= "<div class='tsext-video-content'>";
	$content .= tsext_render_video_content();
	$content .= "</div>";

	$content .= "</div>";

	return $content;
}

/**
 * Register shortcodes
 */
function tsext_register_shortcodes() {
	add_shortcode( TSEXT_SHORTCODE_NAME, 'tsext_render_shortcode' );
}

add_action( 'init', 'tsext_register_shortcodes' );

/**
 * Enqueue scripts
 */
function tsext_wp_enqueue_scripts() {
	global $post;

	if (
		is_object( $post )
		&& has_shortcode( $post->post_content, TSEXT_SHORTCODE_NAME )
	) {
		wp_enqueue_script( 'tsext-script', get_stylesheet_directory_uri() . '/assets/js/scripts.js', [ 'jquery' ] );

		$translation_array = [
			'admin_ajax_url' => admin_url( 'admin-ajax.php' )
		];
		wp_localize_script( 'tsext-script', 'tsext_data', $translation_array );
	}
}

add_action( 'wp_enqueue_scripts', 'tsext_wp_enqueue_scripts' );

/**
 * Ajax action for get video
 */
function tsext_get_video() {
	$succes = false;
	$result = __( 'Failed to get video', 'twentyseventeenext' );

	if ( isset( $_POST['cat'] ) && ! is_null( $_POST['cat'] ) ) {
		$category = $_POST['cat'];
		$result   = tsext_render_video_content( $category );
		$succes   = true;
	}

	exit( json_encode( [
		'success' => $succes,
		'result'  => $result,
	] ) );
}

add_action( 'wp_ajax_tsext_get_video', 'tsext_get_video' );
add_action( 'wp_ajax_nopriv_tsext_get_video', 'tsext_get_video' );