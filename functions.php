<?php

define( 'TSEXT_THEME_NAME', 'twentyseventeenext' );
define( 'TSEXT_THEME_PATH', get_theme_root() . '/' . TSEXT_THEME_NAME );
define( 'TSEXT_SHORTCODE_NAME', 'video-list' );

/**
 * Add localization
 */
function tsext_setup_theme() {
	load_child_theme_textdomain( 'twentyseventeenext', get_stylesheet_directory() . '/languages' );
}

add_action( 'after_setup_theme', 'tsext_setup_theme' );

/**
 * Implement the custom post types and the taxonomies
 */
require TSEXT_THEME_PATH . '/inc/register.php';

/**
 * Implement the shortcode functions
 */
require TSEXT_THEME_PATH . '/inc/shortcode.php';