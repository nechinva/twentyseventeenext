(function ($) {
    $(document).ready(function () {
        $('#tsext-category').on('change', function () {

            if ($(this).hasClass('disabled')) {
                return false;
            }
            $(this).addClass('disabled');

            $.post(
                tsext_data.admin_ajax_url,
                {
                    action: 'tsext_get_video',
                    cat: $(this).val()
                },
                function (data) {
                    $('#tsext-category').removeClass('disabled');

                    if (data !== null && data.success === true) {
                        $('.tsext-video-content').html(data.result);
                    } else {
                        alert(data.result);
                    }
                }, 'json'
            );
        });
    });

})(jQuery);
